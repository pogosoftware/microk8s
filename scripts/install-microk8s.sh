#!/bin/bash
echo "Installing microk8s..."
sudo snap install microk8s --classic

echo "Ensuring microk8s is started..."
microk8s.start

until $(curl --output /dev/null --silent --head --fail http://localhost:8080); do
    echo "Waiting for microk8s..."
    sleep 1
done

echo "Create kubectl alias..."
sudo snap alias microk8s.kubectl kubectl

echo "Enabling storage and metrics-server..."
microk8s.enable storage metrics-server